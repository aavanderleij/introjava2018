package nl.bioinf.nomi.hello;


/**
 * Models a Horse. you know, found in sausages.
 * @author Michiel Noback
 */
public class Horse {
    int weightInKilograms;
    String furColor = "brown";

    /**
     * Will make the horse gallop at the given speed.
     * @param speedInKmPerHour the speed
     */
    public void gallop(double speedInKmPerHour) {
        //simulate galloping
        System.out.println("Horse galloping at " + speedInKmPerHour + " km per hour");
    }
}
