package nl.bioinf.nomi.testtube;

import java.util.Properties;

public class TestTube {
    public static void main(String[] args) {
        System.out.println("Starting simulation...");
        TestTube tube = new TestTube();
        tube.startSimulation();
    }

    private void startSimulation() {
        Cell cell = new Cell();
        cell.grow();
    }
}
